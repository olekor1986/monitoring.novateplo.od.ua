<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SourceTableSeeder extends Seeder
{
    protected $sources = [
        [
            'user_id' => 1,
            'type' => 'котельная',
            'address' => 'Source 1',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "60", "min": "35", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "45", "min": "25", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": null, "min": null, "name": "P2", "type": "bool", "enable": "on", "dimension": null}, "field3": {"max": null, "min": null, "name": "Electro", "type": "bool", "enable": "on", "dimension": null}, "field4": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 1,
            'type' => 'котельная',
            'address' => 'Source 2',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "60", "min": "35", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "45", "min": "25", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": null, "min": null, "name": "P2", "type": "bool", "enable": "on", "dimension": null}, "field3": {"max": null, "min": null, "name": "Electro", "type": "bool", "enable": "on", "dimension": null}, "field4": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 1,
            'type' => 'котельная',
            'address' => 'Source 3',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "60", "min": "35", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "45", "min": "25", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": null, "min": null, "name": "P2", "type": "bool", "enable": "on", "dimension": null}, "field3": {"max": null, "min": null, "name": "Electro", "type": "bool", "enable": "on", "dimension": null}, "field4": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 1,
            'type' => 'котельная',
            'address' => 'Source 4',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "60", "min": "35", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "45", "min": "25", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": null, "min": null, "name": "P2", "type": "bool", "enable": "on", "dimension": null}, "field3": {"max": null, "min": null, "name": "Electro", "type": "bool", "enable": "on", "dimension": null}, "field4": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 1,
            'type' => 'котельная',
            'address' => 'Source 5',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "60", "min": "35", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "45", "min": "25", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": null, "min": null, "name": "P2", "type": "bool", "enable": "on", "dimension": null}, "field3": {"max": null, "min": null, "name": "Electro", "type": "bool", "enable": "on", "dimension": null}, "field4": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 2,
            'type' => 'подкачка',
            'address' => 'Source 6',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "25", "min": "0", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "25", "min": "0", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": 6.0, "min": 3.5, "name": "P2", "type": "float", "enable": "on", "dimension": "bar"}, "field3": {"max": 1, "min": 0, "name": "P1", "type": "float", "enable": "on", "dimension": "bar"}, "field4": {"max": null, "min": null, "name": "Level", "type": "bool", "enable": "on", "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 2,
            'type' => 'котельная',
            'address' => 'Source 7',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "25", "min": "0", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "25", "min": "0", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": 6.0, "min": 3.5, "name": "P2", "type": "float", "enable": "on", "dimension": "bar"}, "field3": {"max": 1, "min": 0, "name": "P1", "type": "float", "enable": "on", "dimension": "bar"}, "field4": {"max": null, "min": null, "name": "Level", "type": "bool", "enable": "on", "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 3,
            'type' => 'подкачка',
            'address' => 'Source 8',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "25", "min": "0", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "25", "min": "0", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": 6.0, "min": 3.5, "name": "P2", "type": "float", "enable": "on", "dimension": "bar"}, "field3": {"max": 1, "min": 0, "name": "P1", "type": "float", "enable": "on", "dimension": "bar"}, "field4": {"max": null, "min": null, "name": "Level", "type": "bool", "enable": "on", "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 3,
            'type' => 'котельная',
            'address' => 'Source 9',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "25", "min": "0", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "25", "min": "0", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": 6.0, "min": 3.5, "name": "P2", "type": "float", "enable": "on", "dimension": "bar"}, "field3": {"max": 1, "min": 0, "name": "P1", "type": "float", "enable": "on", "dimension": "bar"}, "field4": {"max": null, "min": null, "name": "Level", "type": "bool", "enable": "on", "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 4,
            'type' => 'подкачка',
            'address' => 'Source 10',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "25", "min": "0", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "25", "min": "0", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": 6.0, "min": 3.5, "name": "P2", "type": "float", "enable": "on", "dimension": "bar"}, "field3": {"max": 1, "min": 0, "name": "P1", "type": "float", "enable": "on", "dimension": "bar"}, "field4": {"max": null, "min": null, "name": "Level", "type": "bool", "enable": "on", "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 4,
            'type' => 'котельная',
            'address' => 'Source 11',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "25", "min": "0", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "25", "min": "0", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": 6.0, "min": 3.5, "name": "P2", "type": "float", "enable": "on", "dimension": "bar"}, "field3": {"max": 1, "min": 0, "name": "P1", "type": "float", "enable": "on", "dimension": "bar"}, "field4": {"max": null, "min": null, "name": "Level", "type": "bool", "enable": "on", "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 5,
            'type' => 'подкачка',
            'address' => 'Source 12',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "25", "min": "0", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "25", "min": "0", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": 6.0, "min": 3.5, "name": "P2", "type": "float", "enable": "on", "dimension": "bar"}, "field3": {"max": 1, "min": 0, "name": "P1", "type": "float", "enable": "on", "dimension": "bar"}, "field4": {"max": null, "min": null, "name": "Level", "type": "bool", "enable": "on", "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ],
        [
            'user_id' => 5,
            'type' => 'котельная',
            'address' => 'Source 13',
            'alarm' => 1,
            'fields' => '{"field0": {"max": "25", "min": "0", "name": "T1", "type": "float", "enable": "on", "dimension": "*C"}, "field1": {"max": "25", "min": "0", "name": "T2", "type": "float", "enable": "on", "dimension": "*C"}, "field2": {"max": 6.0, "min": 3.5, "name": "P2", "type": "float", "enable": "on", "dimension": "bar"}, "field3": {"max": 1, "min": 0, "name": "P1", "type": "float", "enable": "on", "dimension": "bar"}, "field4": {"max": null, "min": null, "name": "Level", "type": "bool", "enable": "on", "dimension": null}, "field5": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field6": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field7": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field8": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}, "field9": {"max": null, "min": null, "name": null, "type": null, "enable": null, "dimension": null}}'
        ]
    ];

    public function run()
    {
        DB::table('sources')->insert($this->sources);
    }
}

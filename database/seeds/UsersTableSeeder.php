<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
        protected $users = [
        [
            'name' => 'olekor1986',
            'email' => 'olekor1986@gmail.com',
            'password' => '123456Qw',
            'role' => 'admin',
            'phone' => '+380989920182',
            'first_name' => 'Олег',
            'last_name' => 'Коровенко',
            'telegram_id' => '954649843'
        ],
        [
            'name' => 'user_1',
            'email' => 'user_1@mail.com',
            'password' => '123456Qw',
            'role' => 'user',
            'phone' => '+380999999999',
            'first_name' => 'UserName',
            'last_name' => 'UserLastName',
            'telegram_id' => ''
        ],
        [
            'name' => 'user_2',
            'email' => 'user_2@mail.com',
            'password' => '123456Qw',
            'role' => 'user',
            'phone' => '+380999999999',
            'first_name' => 'UserName',
            'last_name' => 'UserLastName',
            'telegram_id' => ''
        ],
        [
            'name' => 'user_3',
            'email' => 'user_3@mail.com',
            'password' => '123456Qw',
            'role' => 'user',
            'phone' => '+380999999999',
            'first_name' => 'UserName',
            'last_name' => 'UserLastName',
            'telegram_id' => ''
        ],
        [
            'name' => 'user_4',
            'email' => 'user_4@mail.com',
            'password' => '123456Qw',
            'role' => 'user',
            'phone' => '+380999999999',
            'first_name' => 'UserName',
            'last_name' => 'UserLastName',
            'telegram_id' => ''
        ],
        [
            'name' => 'user_5',
            'email' => 'user_5@mail.com',
            'password' => '123456Qw',
            'role' => 'user',
            'phone' => '+380999999999',
            'first_name' => 'UserName',
            'last_name' => 'UserLastName',
            'telegram_id' => ''
        ],
        [
            'name' => 'user_6',
            'email' => 'user_6@mail.com',
            'password' => '123456Qw',
            'role' => 'user',
            'phone' => '+380999999999',
            'first_name' => 'UserName',
            'last_name' => 'UserLastName',
            'telegram_id' => ''
        ]
    ];
        /**
         * Run the database seeds.
         *
         * @return void
         */

        public function run()
    {
        $array = [];
        foreach ($this->users as $key => $value) {
            $array[$key]['name'] = $value['name'];
            $array[$key]['email'] = $value['email'];
            $array[$key]['api_token'] = md5($value['email']);
            $array[$key]['password'] = Hash::make($value['password']);
            $array[$key]['role'] = $value['role'];
            $array[$key]['phone'] = $value['phone'];
            $array[$key]['first_name'] = $value['first_name'];
            $array[$key]['last_name'] = $value['last_name'];
            $array[$key]['telegram_id'] = $value['telegram_id'];
        }
        DB::table('users')->insert($array);
    }
}

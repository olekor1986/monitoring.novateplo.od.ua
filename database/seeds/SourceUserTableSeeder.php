<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SourceUserTableSeeder extends Seeder
{
    protected $source_user =
        [
            [
                'user_id' => 1,
                'source_id' => 1
            ],
            [
                'user_id' => 1,
                'source_id' => 2
            ],
            [
                'user_id' => 1,
                'source_id' => 3
            ],
            [
                'user_id' => 1,
                'source_id' => 4
            ],
            [
                'user_id' => 1,
                'source_id' => 5
            ],
            [
                'user_id' => 1,
                'source_id' => 6
            ],
            [
                'user_id' => 1,
                'source_id' => 7
            ],
            [
                'user_id' => 5,
                'source_id' => 5
            ],
            [
                'user_id' => 5,
                'source_id' => 6
            ],
            [
                'user_id' => 2,
                'source_id' => 7
            ],
            [
                'user_id' => 2,
                'source_id' => 8
            ]
        ];
    public function run()
    {
        DB::table('source_user')->insert($this->source_user);
    }
}

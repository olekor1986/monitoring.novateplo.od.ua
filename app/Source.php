<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $fillable = ['type', 'address', 'alarm', 'fields', 'user_id'];

    public function user(){
        return $this->belongsToMany(User::class);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{

    public function handle($request, Closure $next)
    {
        if(isset(Auth::user()->role)) {
            if (Auth::user()->role != 'admin') {
                return redirect('/');
            }
        } else {
            return redirect('/login');
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(isset(Auth::user()->role)){
            if(Auth::user()->role == 'admin'){
                return redirect('/admin');
            }
        } else {
                return redirect('/login');
        }
        return view('home.index');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $users = User::all()->load('source');
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $user = $request->all();
        User::create($user);
        return redirect('/admin/users');
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $user_data = $request->all();
        $user->update($user_data);
        return redirect('/admin/users');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect('/admin/users');
    }
}

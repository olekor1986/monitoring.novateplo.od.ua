<?php

namespace App\Http\Controllers;

use App\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SourceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        $sources = [];

        if ( Auth::user()->role == 'user') {
            $sources = Source::all()->where('user_id', 'like', Auth::user()->id);
        }
        return view('sources.index', compact('sources'));
    }


    public function create()
    {
        return view('sources.create');
    }


    public function store(Request $request)
    {
        $source = $request->all();
        $source['fields'] = '
            {
                "field_0":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""},
                "field_1":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""},
                "field_2":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""},
                "field_3":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""},
                "field_4":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""},
                "field_5":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""},
                "field_6":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""},
                "field_7":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""},
                "field_8":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""},
                "field_9":{"enable":"off","name":"","dimension":"","type":"","min":"","max":""}
            }';
        Source::create($source);
        return redirect('/sources');
    }


    public function show(Source $source)
    {
        //
    }


    public function edit(Source $source)
    {
        $fields_json = $source['fields'];
        $fields = json_decode($source['fields'], true);

        return view('sources.edit', compact('source', 'fields', 'fields_json'));
    }


    public function update(Request $request, Source $source)
    {
        $sourceData = $request->all();
        $fieldsData = [];
        foreach ($sourceData as $key => $item){
            if(preg_match('/^field.*?/', $key)){
                $fieldsData[$key] = $item;
                }
            }

        $fieldsArray= [];
        for($i = 0; $i < 10; $i++) {
            if (isset($fieldsData['field' . $i . '_enable'])){
                $fieldsArray['field' . $i]['enable'] = $fieldsData['field' . $i . '_enable'];
            } else {
                $fieldsArray['field' . $i]['enable'] = null;
            }
            $fieldsArray['field' . $i]['name'] = $fieldsData['field' . $i . '_name'];
            $fieldsArray['field' . $i]['dimension'] = $fieldsData['field' . $i . '_dimension'];
            $fieldsArray['field' . $i]['type'] = $fieldsData['field' . $i . '_type'];
            $fieldsArray['field' . $i]['min'] = $fieldsData['field' . $i . '_min'];
            $fieldsArray['field' . $i]['max'] = $fieldsData['field' . $i . '_max'];
        }
        $sourceData['fields'] = json_encode($fieldsArray);
        $source->update($sourceData);
        return back();
    }


    public function destroy(Source $source)
    {
        $source->delete();
        return redirect('/sources');
    }
}

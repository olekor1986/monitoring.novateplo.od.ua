<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function React\Promise\reduce;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $user = $request->all();
        $user['api_token'] = md5($user['email']);
        User::create($user);
        return redirect('/users');
    }

    public function edit(User $user)
    {
        if (Auth::user()->id != $user->id){
            return redirect('/');
        }
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $user_data = $request->all();
        $user->update($user_data);
    }

}

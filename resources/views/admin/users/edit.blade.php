@extends('templates.admin')

@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2 class="mt-5">Edit user {{$user->name}}</h2>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="card border-primary mb-3">
        <div class="card-body">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" action="/users/{{$user->id}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Login:
                        <input type="text" value="{{$user->name}}" class="form-control" name="name">
                    </label>
                </div>
                <div class="form-group">
                    <label>E-mail:
                        <input type="email" value="{{$user->email}}"  class="form-control" name="email">
                    </label>
                </div>
                <div class="form-group">
                    <label>First Name:
                        <input type="text"  value="{{$user->first_name}}" class="form-control" name="first_name">
                    </label>
                </div>
                <div class="form-group">
                    <label>Last Name:
                        <input type="text"  value="{{$user->last_name}}" class="form-control" name="last_name">
                    </label>
                </div>
                <div class="form-group">
                    <label>Phone:
                        <input type="phone"  value="{{$user->phone}}" class="form-control" name="phone">
                    </label>
                </div>
                @if(Auth::user()->role == 'admin')
                <div class="form-group">
                    <label>Id:
                        <input type="text" value="{{$user->id}}" class="form-control" name="id">
                    </label>
                </div>
                <div class="form-group">
                    <label>Telegram Id:
                        <input type="text"  value="{{$user->telegram_id}}" class="form-control" name="telegram_id">
                    </label>
                </div>
                @endif
                <button class="btn btn-success">Edit</button>
            </form>
        </div>
    </div>
@endsection


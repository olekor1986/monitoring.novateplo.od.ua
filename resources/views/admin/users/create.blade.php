@extends('templates.admin')

@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2 class="mt-5">Add new User</h2>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="card border-primary mb-3">
        <div class="card-body">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" action="/users" method="post">
                @csrf
                <div class="form-group">
                    <label>Login:
                        <input type="text" class="form-control" name="name">
                    </label>
                </div>
                <div class="form-group">
                    <label>E-mail:
                        <input type="email" class="form-control" name="email">
                    </label>
                </div>
                <div class="form-group">
                    <label>Password:
                        <input type="password" class="form-control" name="password">
                    </label>
                </div>
                <div class="form-group">
                    <label>First Name:
                        <input type="text" class="form-control" name="first_name">
                    </label>
                </div>
                <div class="form-group">
                    <label>Last Name:
                        <input type="text" class="form-control" name="last_name">
                    </label>
                </div>
                <div class="form-group">
                    <label>Phone:
                        <input type="phone" class="form-control" name="phone">
                    </label>
                </div>
                <div class="form-group">
                    <label>Telegram Id:
                        <input type="text" class="form-control" name="telegram_id">
                    </label>
                </div>
                <button class="btn btn-success">Create</button>
            </form>
        </div>
    </div>
@endsection


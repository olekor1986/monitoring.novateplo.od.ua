@extends('templates.admin')

@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2 class="mt-5">Пользователи</h2>
                <a href="/users/create" class="btn btn-sm btn-primary" role="button">Add new user</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <tr>
                        <th>ID</th>
                        <th>Логин</th>
                        <th>E-mail</th>
                        <th>ФИО</th>
                        <th>Роль</th>
                        <th>Api Token</th>
                        <th>Объекты</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($users as $user)
                             <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    {{$user->last_name . ' '}}
                                    {{$user->first_name}}
                                </td>
                                <td>{{$user->role}}</td>
                                <td>{{$user->api_token}}</td>
                                <td>
                                    @foreach($user->source as $source)
                                        {{ $source->address }}
                                    @endforeach
                                </td>
                                <td>
                                    <a href="/admin/users/{{$user->id}}/edit" class="btn btn-sm btn-warning" role="button">Edit</a>
                                </td>
                                <td>
                                    <form action="/admin/users/{{$user->id}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-sm btn-danger">X</button>
                                    </form>
                                </td>
                            </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection

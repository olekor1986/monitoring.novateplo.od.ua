<!DOCTYPE html>
<html lang='ua'>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>
			@yield('title')
		</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
		<style type="text/css">
			body {
				background: url('/assets/img/bg1.jpg');
			}
			.navbar {
				padding:10px;
				background: #2B6463;
			}
			.content {
				margin-top: 5%;
			}
		</style>
	</head>
	<body>
	<nav class="navbar navbar-expand-lg bg-light>
	  <div class="container-fluid">
	    <a class="navbar-brand" href="/">Monitoring</a>
	    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
	      <div class="navbar-nav">
	        <a class="nav-link" href="/sources">My Sources</a>
	        <a class="nav-link" href="#">Reports</a>
	         <ul class="navbar-nav dropdup-menu-left">
                @if(Auth::check())
                    <li class="nav-item active">
                        <a class="navbar__item nav-link" href="/user">{{Auth::user()->name}}</a>
                    </li>
                    <form action="/logout" method="post">
                        {{ csrf_field() }}
                        <button class="btn btn-dark">Logout</button>
                    </form>
                @else
                    <li class="nav-item active">
                        <a class="navbar__item nav-link" href="/register">Register</a>
                    </li>
                @endif
            </ul>
	      </div>

	    </div>
	  </div>
	</nav>
	<div class="header">
		@yield('header')
	</div>
	<main class="content">
			@yield('content')		
	</main>
	<div class="footer">
		@yield('footer')
	</div>
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous"></script>
	</body>
</html>	

@extends('templates.dashboard')

@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2 class="mt-5">Add Source Fields</h2>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="card border-primary mb-3">
        <div class="card-body">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" action="/source_fields" method="post">
                @csrf
                    @for($i = 0; $i < 10; $i++)
                    <div class="form-group">
                        <label>Field_{{ $i }}
                            <input type="checkbox" class="#" name="field_{{ $i }}_enable">
                            <input type="text" class="form-control" name="field_{{ $i }}_name" placeholder="Name">
                            <input type="text" class="form-control" name="field_{{ $i }}_dimension" placeholder="Dimension">
                            <select name="field_{{ $i }}_type" class="form-control">
                                <option selected hidden disabled>Выберите тип...</option>
                                <option value="int">INT</option>
                                <option value="float">FLOAT</option>
                                <option value="bool">BOOL</option>
                            </select>
                            <input type="text" class="form-control" name="field_{{ $i }}_min" placeholder="min">
                            <input type="text" class="form-control" name="field_{{ $i }}_max" placeholder="max">
                        </label>
                    </div>
                    @endfor
            <button class="btn btn-success">Create</button>
            </form>
        </div>
    </div>
@endsection


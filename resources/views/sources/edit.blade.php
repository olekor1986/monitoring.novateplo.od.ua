@extends('templates.main')

@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2 class="mt-5">Edit source {{$source->address}}</h2>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="card border-primary mb-3">
        <div class="card-body">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" action="/sources/{{ $source->id }}" method="post">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label>Type:
                            <select name="type" class="form-control" required>
                                <option value="{{ $source->type }}" selected>{{ $source->type }}</option>
                                <option value="котельная">котельная</option>
                                <option value="подкачка">подкачка</option>
                            </select>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Address:
                            <input type="text" class="form-control" name="address" value="{{ $source->address }}">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Alarm:
                            <select name="alarm" class="form-control" required>
                                <option value="{{ $source->alarm }}"  selected>{{ $source->alarm }}</option>
                                <option value="1" selected>1</option>
                                <option value="0">0</option>
                            </select>
                        </label>
                    </div>
                @foreach($fields as $key => $field)
                    <div class="form-group">
                        <label>{{ ucfirst($key) }}
                            @if($field['enable'] == 'on')
                                <input type="checkbox" name="{{ $key }}_enable" checked>
                            @else
                                <input type="checkbox" name="{{ $key }}_enable">
                            @endif
                            <input type="text" class="form-control" name="{{ $key }}_name" placeholder="Name" value="{{ $field['name'] }}">
                            <input type="text" class="form-control" name="{{ $key }}_dimension" placeholder="Dimension" value="{{ $field['dimension'] }}">
                            <select name="{{ $key }}_type" class="form-control">
                                <option value="{{ $field['type'] }}" selected>{{ $field['type'] }}</option>
                                <option value="int">int</option>
                                <option value="float">float</option>
                                <option value="bool">bool</option>
                            </select>
                            <input type="text" class="form-control" name="{{ $key }}_min" placeholder="min" value="{{ $field['min'] }}">
                            <input type="text" class="form-control" name="{{ $key }}_max" placeholder="max" value="{{ $field['max'] }}">
                        </label>
                    </div>
                @endforeach
                <button class="btn btn-success">Edit Source Data</button>
            </form>
        </div>
    </div>
@endsection


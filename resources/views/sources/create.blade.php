@extends('templates.main')

@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2 class="mt-5">Add new Source</h2>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="card border-primary mb-3">
        <div class="card-body">
            @if($errors->all())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class ="form-horizontal" action="/sources" method="post">
                @csrf
                <input type="text" name="user_id" value="{{ Auth::user()->id }}" hidden>
                <div class="form-group">
                    <label>Type:
                        <select name="type" class="form-control" required>
                            <option selected hidden disabled>Выберите тип...</option>
                            <option value="котельная">котельная</option>
                            <option value="подкачка">подкачка</option>
                        </select>
                    </label>
                </div>
                <div class="form-group">
                    <label>Address:
                        <input type="text" class="form-control" name="address">
                    </label>
                </div>
                <div class="form-group">
                    <label>Alarm:
                        <select name="alarm" class="form-control" required>
                            <option value="1" selected>1</option>
                            <option value="0">0</option>
                        </select>
                    </label>
                </div>
            <button class="btn btn-success">Create</button>
            </form>
        </div>
    </div>
@endsection


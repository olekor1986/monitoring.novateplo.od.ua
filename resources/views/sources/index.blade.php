@extends('templates.main')

@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2 class="mt-5">Sources</h2>
                <a href="/sources/create" class="btn btn-sm btn-primary" role="button">Add new Source</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="card border-primary">
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <tr>
                        <th>ID</th>
                        <th>UserID</th>
                        <th>Type</th>
                        <th>Address</th>
                        <th>Alarm</th>
                        <th>Users</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($sources as $source)
                             <tr>
                                <td>{{$source->id}}</td>
                                <td>{{$source->user_id}}</td>
                                <td>{{$source->type}}</td>
                                <td>{{$source->address}}</td>
                                <td>{{$source->alarm}}</td>
                                <td>
                                    @foreach($source->user as $user)
                                        {{ $user->first_name }}
                                    @endforeach
                                </td>
                                <td>
                                    <a href="/sources/{{$source->id}}/edit" class="btn btn-sm btn-warning" role="button">Edit</a>
                                </td>
                                <td>
                                    <form action="/sources/{{$source->id}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-sm btn-danger">X</button>
                                    </form>
                                </td>
                            </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection

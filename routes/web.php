<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::prefix('admin')->group(function () {
    Route::get('/', 'Admin\AdminController@index');
    Route::resource('/users', 'Admin\AdminUserController')->only('index', 'create',
        'store', 'edit', 'update', 'destroy');
    Route::resource('/sources', 'Admin\AdminSourceController')->only('index', 'create',
        'store', 'edit', 'update', 'destroy');
});

Route::get('/', 'HomeController@index');
Route::resource('users', 'UserController')->only('edit', 'update');
Route::resource('sources', 'SourceController')->only('index', 'create',
    'store', 'edit', 'update', 'destroy');
Route::resource('/source_fields', 'SourceFieldsController@update');

